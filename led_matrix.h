#ifndef LED_MATRIX_H
#define LED_MATRIX_H

/* Initialize the led matrix:

   buffer1, buffer2:
   The led matrix driver is double buffered: while one buffer is being
   filled by the user software, the other buffer is being drawn to the led
   matrix. Both buffers must be provided by the user software.

   rows, cols: a buffer size

   turn_on_singe_led(row, col):
   function call for turning one single led on

   turn_off_all_leds:     
   function call for turning off all leds
*/
void led_matrix_init( unsigned char *buffer1,
                      unsigned char *buffer2,
                      unsigned int rows, unsigned int cols, 
                      void (*turn_on_singe_led)(unsigned int, unsigned int),
                      void (*turn_off_all_leds)(void) );


/* Update the led matrix.
   This function must be called periodically and fast enough for the led matrix
   to be drawn without blinking. */
void led_matrix_update( void );

/* Tell the user software whether the matrix has finished drawing
   and whether a call to the draw function can be made. */
unsigned int led_matrix_is_ready( void );

/* Get a buffer the user software can fill */
unsigned char *led_matrix_get_empty_buffer( void );

/* Tells the led matrix driver to draw the current user software buffer.
   The user software should check whether a call to this function can be
   made, by callling the 'is ready' function. */
void led_matrix_draw( unsigned int frames );

#endif /* LED_MATRIX_H */

